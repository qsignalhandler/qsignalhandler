#include <QCoreApplication>

#include <signal.h>
#include "qsignalhandler.h"

class SignalListener : public QObject
{
    Q_OBJECT

public slots:
    void onSignal(QSignalHandler::PosixSignal signal)
    {
        qDebug() << "Received signal" << signal;
    }
};

#include "main.moc"

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    
    // Create a signal handler
    QSignalHandler signalHandler;

    // Create some qobject that will respond to the signal
    SignalListener signalListener;

    // Connect signals
    QObject::connect(&signalHandler, SIGNAL(signal(QSignalHandler::PosixSignal)),
                     &signalListener, SLOT(onSignal(QSignalHandler::PosixSignal)));

    // Tell signal handler to listen to some signals
    signalHandler.listenToSignal(QSignalHandler::Sigterm);
    signalHandler.listenToSignal(QSignalHandler::Sighup);

    return a.exec();
}
