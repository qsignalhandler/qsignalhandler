#-------------------------------------------------
#
# Project created by QtCreator 2013-07-27T15:50:16
#
#-------------------------------------------------

QT       += core

QT       -= gui

TARGET = qsignalhandler
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp \
    qsignalhandler.cpp

HEADERS += \
    qsignalhandler.h
