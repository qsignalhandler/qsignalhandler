#include "qsignalhandler.h"

int QSignalHandler::sigFd[2] = {-1, -1};

QSignalHandler::QSignalHandler()
{
    if (::socketpair(AF_UNIX, SOCK_STREAM, 0, sigFd))
    {
        qFatal("Couldn't create socketpair");
    }

    sn = new QSocketNotifier(sigFd[1], QSocketNotifier::Read, this);
    if (!connect( sn, SIGNAL( activated(int) ), this, SLOT(handleSig())))
    {
        qFatal("Couldn't connect socketnotifier");
    }
}

void QSignalHandler::listenToSignal(QSignalHandler::PosixSignal signal)
{
    struct sigaction sigAction;

    sigAction.sa_handler = signalHandler;
    sigemptyset(&sigAction.sa_mask);
    sigAction.sa_flags = 0;
    sigAction.sa_flags |= SA_RESTART;

    if (sigaction(int(signal), &sigAction, 0) != 0)
    {
        qFatal("Couldn't register sigaction");
        return;
    }
}

void QSignalHandler::signalHandler(int s)
{
    char a = char(s);
    ::write(sigFd[0], &a, sizeof(a));
}


void QSignalHandler::handleSig()
{
    sn->setEnabled(false);
    char tmp;
    ::read(sigFd[1], &tmp, sizeof(tmp));

    emit signal(PosixSignal(tmp));

    sn->setEnabled(true);
}
