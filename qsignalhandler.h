#ifndef POSIXSIGNAL_H
#define POSIXSIGNAL_H

#include <signal.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/socket.h>

#include <QtCore>

class QSignalHandler : public QObject
{
    Q_OBJECT
public:
    enum PosixSignal
    {
        // Dummy for value 0
        _Nosig,
        // Matching the order in bits/signum.h
        Sighup, Sigint, Sigquit, Sigill, Sigtrap, Sigabrt, Sigbus, Sigfpe, Sigkill, Sigusr1, Sigsegv, Sigusr2, Sigpipe, Sigalrm, Sigterm, Sigstkflt, Sigchld, Sigcont, Sigstop, Sigtstp, Sigttin, Sigttou, Sigurg, Sigxcpu, Sigxfsz, Sigvtalrm, Sigprof, Sigwinch, Sigio, Sigpwr, Sigsys,
        // Signals with duplicate number
        Sigiot = Sigabrt, Sigcld = Sigchld, Sigpoll = Sigio, Sigunused = Sigsys
    };

    QSignalHandler();

    void listenToSignal(PosixSignal signal);

private:
    // Unix signal handlers.
    static void signalHandler(int s);

    static int sigFd[2];

    QSocketNotifier *sn;

signals:
    void signal(QSignalHandler::PosixSignal);

private slots:
    void handleSig();
};

#endif // POSIXSIGNAL_H
